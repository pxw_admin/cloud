# 邮件

##### 发送邮件

```js
const result = await module.sendMail({  
    from: '1604583867@qq.com', // 发送者  
    to: '1406281912@qq.com', // 接受者,可以同时发送多个,以逗号隔开  
    subject: '发送邮件测试', // 标题  
    text: 'Hello World', // 文本  
    html: `<h2>Hello World</h2>`   
});
```